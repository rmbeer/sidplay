#ifndef _UNIX_SIDINT_H
#define _UNIX_SIDINT_H 1
#ifndef _GENERATED_STDINT_H
#define _GENERATED_STDINT_H "libsidplay 2.1.1"
/* generated using a gnu compiler version gcc (Debian 4.3-20080219-1) 4.3.0 20080219 (prerelease) [gcc-4_3-branch revision 132456] Copyright (C) 2007 Free Software Foundation, Inc. This is free software; see the source for copying conditions. There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. */

#include <stdint.h>


/* system headers have good uint64_t */
#ifndef _HAVE_UINT64_T
#define _HAVE_UINT64_T
#endif

  /* once */
#endif
#endif
